#include <string>

#include "gtest/gtest.h"
#include "queue.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation;
using namespace testing;


template<typename T>
struct QueueFixture : Test {
    const unsigned MAX = 5;
    Queue<T>       theQueue{MAX};
    T              sampleData;
};
using TypeParams = Types<int, double, string>;
TYPED_TEST_CASE(QueueFixture, TypeParams);

TYPED_TEST(QueueFixture, checkEmpty) {
    EXPECT_TRUE(this->theQueue.empty());
}

TYPED_TEST(QueueFixture, checkPutAndGet) {
    EXPECT_TRUE(this->theQueue.empty());

    this->theQueue.put(this->sampleData);
    EXPECT_FALSE(this->theQueue.empty());

    auto x = this->theQueue.get();
    EXPECT_TRUE(this->theQueue.empty());
    EXPECT_EQ(x, this->sampleData);
}

TYPED_TEST(QueueFixture, checkFull) {
    EXPECT_FALSE(this->theQueue.full());

    while (!this->theQueue.full()) {
        this->theQueue.put(this->sampleData);
    }
    EXPECT_TRUE(this->theQueue.full());

    while (!this->theQueue.empty()) {
        this->theQueue.get();
        EXPECT_FALSE(this->theQueue.full());
    }
    EXPECT_TRUE(this->theQueue.empty());
}

