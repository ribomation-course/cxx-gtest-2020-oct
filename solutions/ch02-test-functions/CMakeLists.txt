cmake_minimum_required(VERSION 3.16)
project(ch02_test_functions)

set(CMAKE_CXX_STANDARD 17)

include(FetchContent)
FetchContent_Declare(gtest
        GIT_REPOSITORY  https://github.com/google/googletest.git
        GIT_TAG         release-1.10.0
)
FetchContent_GetProperties(gest)
if(NOT gtest_POPULATED)
    FetchContent_Populate(gtest)
    set(gtest_disable_pthreads ON CACHE BOOL "" FORCE)
    add_subdirectory(${gtest_SOURCE_DIR}/googletest)
endif()



add_executable(test-app
        numeric-stats.hxx
        test-app.cxx
        )
target_link_libraries(test-app PRIVATE gtest gtest_main)

