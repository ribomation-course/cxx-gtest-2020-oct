# Installation Instructions

In order to do the programming exercises of the course, 
you need to have a decent C++ compiler and an editor.
Although there are many ways to write/compile/run C++
applications, we strongly suggest the following tools
for this course:

* Ubuntu Linux
* GCC/G++ 10
* JetBrains CLion
* CMake
* Google Test

## Install Linux and C++

Follow the instructions below:

* [Linux and C++](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)

## Installation of Google Test

`gtest` is an external library that need to be downloaded and
compiled. You can find the source code repo at GitHub

* [Google Test Repo](https://github.com/google/googletest)

You don't need to install `gtest` before the course, as we will
go through the various installation options during the course
and settle for letting CMake do the download and compilation.

## Installation of CLion

Our favourite IDE for C++ development is CLion. It has very
nice built-in support for running `gtest` tests.

For this course, install the 30 day trial version.

* [JetBrains CLion IDE](https://www.jetbrains.com/clion/)

